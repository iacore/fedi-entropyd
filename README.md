# Fediverse Entropy Daemon

shitpost-powered userspace entropy provider for Linux

default entropy provider: [@LARSNEVERDIES@labyrinth.zone](https://labyrinth.zone/users/LARSNEVERDIES)

```
# Run the daemon
./fedi-entropyd
# Here's a less secure but more shitposter-friendly version
./fedi-entropyd-advanced
```

To use another source of entropy, edit the script.

Here's what you can modify

- fetch from multiple users
- decrease the delay

## But does this work?

Maybe. It is only useful when your system need external entropy.

## Security Considerations

Make sure the list of Fediverse accounts followed is kept secret.

Make sure the account owners are not kidnapped.

Make sure the homeservers used by this program is trustworthy.

